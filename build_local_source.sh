#!/bin/bash

export PROJECT_ROOT="$PWD"
export VERSION="1.0.0"
export K3S_TAG="v1.21.2+k3s1"
export DOCKER_VERSION="20.10.7"
export IMAGE_TAG="v1.21.2-k3s1"

cd base && \
sudo docker build --build-arg DOCKER_VERSION=$DOCKER_VERSION -t registry.gitlab.com/vainkop1/k3s-gpu/base:$VERSION . && \
sudo docker push registry.gitlab.com/vainkop1/k3s-gpu/base:$VERSION

cd $PROJECT_ROOT && \
sudo rm -rf ./k3s && \
git clone --depth 1 https://github.com/rancher/k3s.git -b "$K3S_TAG" && \
sudo docker run -ti -v ${PWD}/k3s:/k3s -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/vainkop1/k3s-gpu/base:1.0.0 sh -c "cd /k3s && make" && \
sudo ls -al k3s/build/out/data.tar.zst

if [ -f $PROJECT_ROOT/k3s/build/out/data.tar.zst ]; then
  echo "File exists! Building!"
  sudo docker build -t registry.gitlab.com/vainkop1/k3s-gpu:$IMAGE_TAG . && \
  sudo docker push registry.gitlab.com/vainkop1/k3s-gpu:$IMAGE_TAG
  echo "Done!"
else
  echo "Error, file does not exist!"
  exit 1
fi
