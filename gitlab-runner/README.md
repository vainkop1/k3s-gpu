# Gitlab Runner with Docker TLS on K3d  
  
1) Generate certificates using provided script:  
  
`./start-docker-with-tls.sh`  

2) Provision a K3d cluster with a folder with client certificates mounted as folder:  
  
`k3d cluster create local --image=registry.gitlab.com/vainkop1/k3s-gpu:v1.21.2-k3s1 --gpus=all -v $HOME/.docker/DOCKER_TLS_CERTDIR:/certs/client`
  
3) Replace `$RUNNER_REGISTRATION_TOKEN` with your token in `helm-values.yml` & deploy the Gitlab a Runner:
  
```
kubectl create namespace gitlab --dry-run=client -o yaml | kubectl apply -f -

kubectl config set-context --current --namespace=gitlab

kubectl create secret generic registry \
  --from-file=.dockerconfigjson=$HOME/.docker/config.json \
  --type=kubernetes.io/dockerconfigjson \
  --dry-run=client -o yaml | kubectl apply -f -

helm repo add gitlab https://charts.gitlab.io && helm repo update
helm upgrade -i gitlab-runner gitlab/gitlab-runner --values=helm-values.yml
```